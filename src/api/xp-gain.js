import http from '../axios.js';

export default {
  getPlayersXpInTime(username, timeInSeconds) {
    return http
      .get(`/api/tracker/player/${username}?time=${timeInSeconds}`)
      .then(response => response.data);
  },
};
