import React, { Component } from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import Aux from './Auxiliary';
import Toolbar from '../components/UI/Toolbar';

import Home from '../containers/Home';
import Map from '../containers/Map';
import Highscores from '../containers/Highscores';
import Downloads from '../containers/Downloads';
import Authenticator from '../containers/Authenticator';
import Patchnotes from '../containers/Patchnotes';
import DevBlog from '../containers/DevBlog';
import Media from '../containers/Media';
import Error from '../components/UI/Error';

export default class Layout extends Component {
  render() {
    return (
      <Aux>
        <Toolbar />
        <main>
          <Switch>
            <Route path="/" exact component={Home} />
            <Route path="/map" exact component={Map} />
            <Route path="/highscores" exact component={Highscores} />
            <Redirect path="/hiscores" to="/highscores" />
            <Route path="/downloads" exact component={Downloads} />
            <Route path="/auth" exact component={Authenticator} />
            <Route path="/patchnotes" exact component={Patchnotes} />
            <Route
              path="/helpwanted"
              component={() => {
                window.location =
                  'https://gitlab.com/vscape/public/issue-tracker/issues?label_name%5B%5D=Help+wanted';
                return null;
              }}
            />
            <Route path="/devblog" exact component={DevBlog} />
            <Route path="/media" exact component={Media} />
            <Route component={Error} />
          </Switch>
        </main>

        <footer>&nbsp;</footer>
      </Aux>
    );
  }
}
