import React from 'react';
import Button from 'react-bootstrap/Button';

const filterButton = props => (
  <Button
    onClick={() => props.clickEvent(props.value)}
    variant={'primary'}
    active={props.active}
    disabled={props.disabled}
  >
    {props.children}
  </Button>
);

export default filterButton;
