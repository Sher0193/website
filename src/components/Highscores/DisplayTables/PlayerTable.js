import React from 'react';

import Spinner from '../../../components/UI/Spinner';
import Aux from '../../../hoc/Auxiliary';
import SkillImage from '../../../components/Highscores/SkillImage';
import ResponsiveTable from '../../UI/ResponsiveTable';

import {
  getValidSkills,
  getValidStats,
  //   getValidBosses,
  formatNumber,
  formatSectionName,
} from '../../../utils/highscores';

function generateSkillsHeader() {
  return (
    <tr>
      <th className={'text-center'}>Skill</th>
      <th className={'text-center'}>Rank</th>
      <th className={'text-center'}>Level</th>
      <th className={'text-center'}>Experience</th>
      <th className={'text-center'}>XP Gain</th>
    </tr>
  );
}

function generateSkillsBody(click, data, xpGain, xpGainIsLoading) {
  return getValidSkills().map(s => {
    const xpDisplay = xpGainIsLoading ? (
      <td>
        <Spinner />
      </td>
    ) : (
      <td className={'text-center'}>{formatNumber(xpGain[s + '_diff'])}</td>
    );
    return (
      <tr onClick={() => click(s)} key={s} style={{ cursor: 'pointer' }}>
        <td className={'text-center'}>
          <SkillImage skill={s} /> {formatSectionName(s)}
        </td>
        <td className={'text-center'}>{formatNumber(data[s + '_rank'])}</td>
        <td className={'text-center'}>{formatNumber(data[s + '_lvl'])}</td>
        <td className={'text-center'}>{formatNumber(data[s + '_xp'])}</td>
        {xpDisplay}
      </tr>
    );
  });
}

// clue scrolls
function generateStatsHeader() {
  return (
    <tr>
      <th className={'text-center'}>Stat</th>
      <th className={'text-center'}>Rank</th>
      <th className={'text-center'}>Score</th>
    </tr>
  );
}

function generateStatsBody(click, data) {
  return getValidStats().map(g => (
    <tr onClick={() => click(g)} key={g} style={{ cursor: 'pointer' }}>
      <td className={'text-center'}>
        <SkillImage skill={g} /> {formatSectionName(g)}
      </td>
      <td className={'text-center'}>{data[g + '_rank']}</td>
      <td className={'text-center'}>{data[g]}</td>
    </tr>
  ));
}

// function generateBossesHeader() {
//   return (
//     <tr>
//       <th className={'text-center'}>Boss</th>
//       <th className={'text-center'}>Kills</th>
//     </tr>
//   );
// }
//
// function generateBossesBody(data) {
//   return getValidBosses().map(g => (
//     <tr key={g} style={{ cursor: 'pointer' }}>
//       <td className={'text-center'}>
//         <SkillImage skill={g} /> {formatSectionName(g)}
//       </td>
//       <td className={'text-center'}>{data[g + '_kills']}</td>
//     </tr>
//   ));
// }

const playerTable = props => (
  <Aux>
    <ResponsiveTable
      header={generateSkillsHeader()}
      body={generateSkillsBody(
        props.clickEvent,
        props.data,
        props.xpGain,
        props.xpGainIsLoading,
      )}
    />

    <ResponsiveTable
      header={generateStatsHeader()}
      body={generateStatsBody(props.clickEvent, props.data)}
    />
  </Aux>
);

//     <ResponsiveTable
//       header={generateBossesHeader()}
//       body={generateBossesBody(props.data)}
//     />

export default playerTable;
