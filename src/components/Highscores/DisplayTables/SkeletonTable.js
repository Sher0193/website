import React from 'react';

import ResponsiveTable from '../../UI/ResponsiveTable';
import Spinner from '../../../components/UI/Spinner';

import {
  HIGHSCORES,
  TRACKER,
  PLAYER,
  COMPARE,
} from '../../../utils/constants.js';

function generateHeader(stat, mode) {
  if (stat) {
    return (
      <tr>
        <th className={'text-right'} style={{ width: '70px' }}>
          Rank
        </th>
        <th className={'text-center'}>Username</th>
        <th className={'text-center'}>Score</th>
      </tr>
    );
  } else if (mode === PLAYER) {
    return (
      <tr>
        <th className={'text-center'}>Skill</th>
        <th className={'text-center'}>Rank</th>
        <th className={'text-center'}>Level</th>
        <th className={'text-center'}>Experience</th>
      </tr>
    );
  } else if (mode === HIGHSCORES) {
    return (
      <tr>
        <th className={'text-right'} style={{ width: '70px' }}>
          Rank
        </th>
        <th className={'text-center'}>Username</th>
        <th className={'text-center'}>Level</th>
        <th className={'text-center'}>Experience</th>
      </tr>
    );
  } else if (mode === COMPARE) {
    return (
      <tr>
        <th className={'text-center'}>Skill</th>
        <th className={'text-center'}>Rank</th>
        <th className={'text-center'}>Level</th>
        <th className={'text-center'}>Experience</th>
        <th />
        <th className={'text-center'}>Rank</th>
        <th className={'text-center'}>Level</th>
        <th className={'text-center'}>Experience</th>
      </tr>
    );
  } else if (mode === TRACKER) {
    return (
      <tr>
        <th className={'text-right'} style={{ width: '70px' }}>
          Rank
        </th>
        <th className={'text-center'}>Username</th>
        <th className={'text-center'}>XP Gained</th>
      </tr>
    );
  }
}

function generateRows(cellCount, rowKey) {
  const cells = Array.from(Array(cellCount).keys()).map(i => {
    return (
      <td key={'hscell' + i}>
        <Spinner />
      </td>
    );
  });
  return (
    <tr key={'hsrow' + rowKey} style={{ height: '33px' }}>
      {cells}
    </tr>
  );
}

function generateSkeleton(stat, mode) {
  const data = Array.from(Array(25).keys());
  if (stat) {
    return data.map(i => generateRows(3, i));
  } else if (mode === COMPARE) {
    return data.map(i => generateRows(8, i));
  } else if (mode === TRACKER) {
    return data.map(i => generateRows(3, i));
  } else {
    return data.map(i => generateRows(4, i));
  }
}

export const skeletonTable = props => (
  <ResponsiveTable
    header={generateHeader(props.stat, props.mode)}
    body={generateSkeleton(props.stat, props.mode)}
  />
);

export default skeletonTable;
