import React, { Component } from 'react';
import { Link } from 'react-router-dom';

import StaticCard from '../../UI/StaticCard';
import CollapsableCard from '../../UI/CollapsableCard';

export default class Patchnotes extends Component {
  state = {
    toggle: [true, false, false, false, false],
  };

  toggle = propID => {
    //Split out the index of the prop by its id
    const idx = propID.split('-')[1];

    //Mutate the toggle array locally
    const toggleStates = this.state.toggle;
    toggleStates[idx] = !toggleStates[idx];

    //Set the state to the new array
    this.setState({ toggle: toggleStates });
  };

  getNoteBody(note, i) {
    const majNotes = note.major.map((n, index) => {
      return (
        <div key={`nbmaj-${i}-${index}`} style={{ color: '#cc0000' }}>
          {n}
        </div>
      );
    });
    const minNotes = note.minor.map((n, index) => {
      return <div key={`nbmin-${i}-${index}`}>- {n}</div>;
    });
    return [...majNotes, ...minNotes];
  }

  generateCards(notes) {
    if (notes === '') {
      return;
    }

    return notes.map((n, i) => (
      <CollapsableCard
        toggle={this.toggle}
        isOpen={this.state.toggle[i]}
        title={n.header}
        body={this.getNoteBody(n, i)}
        id={`pn-${i}`}
        key={`pn-${i}`}
      />
    ));
  }

  generateFooter() {
    if (!this.props.hideFooter) {
      return (
        <p className="text-center">
          <strong>
            Click <Link to={`/patchnotes`}>here</Link> for older patchnotes.
          </strong>
        </p>
      );
    }
  }

  render() {
    return (
      <StaticCard
        title={
          <strong>
            {this.props.title ? this.props.title : 'Recent Patchnotes'}
          </strong>
        }
        headerClasses={'text-center'}
      >
        {this.generateCards(this.props.patchnotes)}

        {this.generateFooter()}
      </StaticCard>
    );
  }
}
