import React from 'react';
import Collapse from 'react-bootstrap/Collapse';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';

const collapsableCard = props => (
  <Card border="secondary" className={'mb-2'}>
    <Card.Header className={`p-1 ${props.headerClasses}`}>
      <Button variant="secondary" onClick={() => props.toggle(props.id)} block>
        {props.title}
      </Button>
    </Card.Header>
    <Collapse in={props.isOpen}>
      <Card.Body className={`${props.bodyClasses}`}>{props.body}</Card.Body>
    </Collapse>
  </Card>
);

export default collapsableCard;
