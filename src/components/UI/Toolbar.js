import React, { Component } from 'react';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';

import Aux from '../../hoc/Auxiliary';
import vscapeLogo from '../../assets/images/logo-sm-normal.png';

export default class Toolbar extends Component {
  render() {
    return (
      <header>
        <Navbar expand="lg" bg="primary" className={'mb-2 p-2'}>
          <LinkContainer exact to="/">
            <Navbar.Brand>
              <img src={vscapeLogo} alt="Vidyascape" title="Vidyascape" />
            </Navbar.Brand>
          </LinkContainer>

          <Navbar.Toggle />

          <Navbar.Collapse>
            <Nav className="ml-auto">
              <Nav.Item>
                <LinkContainer exact to="/downloads">
                  <Nav.Link>
                    <i className={'fas fa-download'} /> Downloads
                  </Nav.Link>
                </LinkContainer>
              </Nav.Item>

              <Nav.Item>
                <LinkContainer exact to="/highscores">
                  <Nav.Link>
                    <i className={'fas fa-chart-line'} /> Highscores
                  </Nav.Link>
                </LinkContainer>
              </Nav.Item>

              <Nav.Item>
                <LinkContainer exact to="/media">
                  <Nav.Link>
                    <i className={'fas fa-camera'} /> Media
                  </Nav.Link>
                </LinkContainer>
              </Nav.Item>

              <Nav.Item>
                <LinkContainer exact to="/map">
                  <Nav.Link>
                    <i className={'fas fa-map'} /> Map
                  </Nav.Link>
                </LinkContainer>
              </Nav.Item>

              <Nav.Item>
                <Nav.Link href="http://vscape.wikidot.com/">
                  <i className={'fas fa-book'} /> Wiki
                </Nav.Link>
              </Nav.Item>

              <NavDropdown
                title={
                  <Aux>
                    <i className={'fa fa-code'} /> Development
                  </Aux>
                }
              >
                <NavDropdown.Item href="https://gitlab.com/vscape/public/issue-tracker/issues">
                  <i className={'fas fa-bug'} /> Bug Tracker
                </NavDropdown.Item>

                <NavDropdown.Item href="https://gitlab.com/vscape/public/website">
                  <i className={'fab fa-gitlab'} /> Website Repo
                </NavDropdown.Item>

                <LinkContainer exact to="/devblog">
                  <NavDropdown.Item>
                    <i className={'fas fa-pen'} /> Dev Blog
                  </NavDropdown.Item>
                </LinkContainer>

                <NavDropdown.Item href="https://gitlab.com/vscape/public/issue-tracker/issues?label_name%5B%5D=Help+wanted">
                  <i className={'fas fa-hands-helping'} /> Help Wanted
                </NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </header>
    );
  }
}
