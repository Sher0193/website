import React from 'react';
import marked from 'marked';
import hljs from 'highlight.js/lib/highlight';
import ruby from 'highlight.js/lib/languages/ruby';
import 'highlight.js/styles/atom-one-light.css';

hljs.registerLanguage('ruby', ruby);

marked.setOptions({
  renderer: new marked.Renderer(),
  highlight: code => {
    return hljs.highlightAuto(code).value;
  },
  sanitize: true,
});

function parseMarkdown(md) {
  return { __html: marked(md) };
}

const markdownRender = props => (
  <div dangerouslySetInnerHTML={parseMarkdown(props.markdown)} />
);

export default markdownRender;
