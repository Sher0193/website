import React from 'react';
import Alert from 'react-bootstrap/Alert';

import Aux from '../../hoc/Auxiliary';

function generateMessage(code) {
  switch (code) {
    case 204:
      return <p> Response 204: no results found.</p>;
    case 404:
      return <p>Error 404: requested information not found.</p>;
    case 408:
      return (
        <p>
          Error 408: request timed out. Please refresh or try a new request.
        </p>
      );
    case 500:
      return (
        <p>Error 500: internal server error, please contact the webmaster.</p>
      );
    default:
      return (
        <p>
          If you think this is a server error, please contact the webmaster.
        </p>
      );
  }
}

const Error = props => (
  <Aux>
    <Alert variant="danger" className="text-center">
      <Alert.Heading>An Issue Has Occurred.</Alert.Heading>
      {generateMessage(props.code)}
    </Alert>
  </Aux>
);

export default Error;
