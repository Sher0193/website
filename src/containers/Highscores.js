import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import XpGainApi from '../api/xp-gain.js';

import Aux from '../hoc/Auxiliary';

import Error from '../components/UI/Error';
import Header from '../components/Highscores/Header/Header';
import Sidebar from '../components/Highscores/Sidebar/Sidebar';
import SkillTable from '../components/Highscores/DisplayTables/SkillTable';
import StatTable from '../components/Highscores/DisplayTables/StatTable';
import PlayerTable from '../components/Highscores/DisplayTables/PlayerTable';
import SkeletonTable from '../components/Highscores/DisplayTables/SkeletonTable';
import CompareTable from '../components/Highscores/DisplayTables/CompareTable';
import XPTrackerTable from '../components/Highscores/DisplayTables/XPTrackerTable';
import PageNavigation from '../components/Highscores/PageNavigation';
import SkillIcon from '../components/Highscores/SkillIcon';

import IronmanLgImg from '../assets/images/icons/ironman_circle_lg.png';
import UltimateIronmanLgImg from '../assets/images/icons/ultimate_circle_lg.png';

import axios from '../axios';

import {
  getValidSkills,
  getValidStats,
  formatSectionName,
  formatPlayerName,
  populatePages,
} from '../utils/highscores';

import {
  DAY_IN_SECONDS,
  WEEK_IN_SECONDS,
  MONTH_IN_SECONDS,
  HIGHSCORES,
  TRACKER,
  PLAYER,
  COMPARE,
  NO_RESP,
  API_TIMEOUT,
} from '../utils/constants.js';

export default class Highscores extends Component {
  state = {
    titleHeader: '',
    tableType: HIGHSCORES,
    tableData: null,
    playerXpGain: null,
    xpGainTime: 86400,
    playerXpGainIsLoading: true,
    tableDataTwo: null,
    ironmanStatus: 0,
    curSection: '',
    curPage: 1,
    curPlayer: '',
    searchPlayer: '',
    searchPlayerOne: '',
    searchPlayerTwo: '',
    error: '',
    endPage: null,
    compareOne: '',
    compareTwo: '',
    compareFilter: 'experience',
    curRequest: 0,
  };

  handlePlayerChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handlePlayerSubmit = event => {
    event.preventDefault();
    const params = new URLSearchParams(this.props.location.search);
    params.delete('skill');
    params.delete('compare');
    params.delete('page');
    params.delete('ironman');
    params.delete('time');
    params.set('player', this.state.searchPlayer);
    this.props.history.push({
      pathname: '/highscores',
      search: params.toString(),
    });
  };

  handleCompareSubmit = event => {
    event.preventDefault();
    const params = new URLSearchParams(this.props.location.search);
    params.delete('skill');
    params.delete('page');
    params.delete('ironman');
    params.delete('time');
    let playerOne = this.state.searchPlayerOne;
    const playerTwo = this.state.searchPlayerTwo;
    if (this.state.searchPlayerOne === '') {
      playerOne = this.state.curPlayer;
    }
    params.set('player', `${playerOne}`);
    params.set('compare', `${playerTwo}`);
    this.props.history.push({
      pathname: '/highscores',
      search: params.toString(),
    });
  };

  handleXpTrackerClick = xpTracker => {
    const params = new URLSearchParams(this.props.location.search);
    params.delete('player');
    params.delete('compare');
    params.delete('page');
    if (xpTracker > 0) {
      params.set('time', xpTracker);
    } else {
      params.delete('time');
    }
    this.props.history.push({
      pathname: '/highscores',
      search: params.toString(),
    });
  };

  handleSectionClick = section => {
    const params = new URLSearchParams(this.props.location.search);
    params.delete('player');
    params.delete('compare');
    params.delete('page');
    params.set('skill', section);
    this.props.history.push({
      pathname: '/highscores',
      search: params.toString(),
    });
  };

  handlePlayerClick = (event, player) => {
    const MIDDLE_CLICK = 2;
    const clickedKey = event.nativeEvent.which;

    const params = new URLSearchParams(this.props.location.search);
    params.delete('skill');
    params.delete('compare');
    params.delete('page');
    params.delete('ironman');
    params.delete('time');
    params.set('player', player);

    if (clickedKey === MIDDLE_CLICK) {
      const win = window.open(`/highscores?${params.toString()}`, '_blank');
      if (win != null) {
        win.focus();
      }
    } else {
      this.props.history.push({
        pathname: '/highscores',
        search: params.toString(),
      });
    }
  };

  handleIronmanClick = ironman => {
    const params = new URLSearchParams(this.props.location.search);
    params.set('ironman', ironman);
    params.delete('page');
    this.props.history.push({
      pathname: '/highscores',
      search: params.toString(),
    });
  };

  handlePageClick = page => {
    const params = new URLSearchParams(this.props.location.search);
    params.set('page', page);
    this.props.history.push({
      pathname: '/highscores',
      search: params.toString(),
    });
  };

  handleXpGainTimeChange = newTime => {
    const params = new URLSearchParams(this.props.location.search);
    params.set('time', newTime);
    this.props.history.push({
      pathname: '/highscores',
      search: params.toString(),
    });
  };

  setXpTracker = (section, page, timeframe, ironman) => {
    const formattedHeader = formatSectionName(
      section +
        (timeframe === DAY_IN_SECONDS
          ? ' Daily'
          : timeframe === WEEK_IN_SECONDS
          ? ' Weekly'
          : timeframe === MONTH_IN_SECONDS
          ? ' Monthly'
          : ' Custom'),
    );
    this.setState({
      tableType: TRACKER,
      curSection: section,
      titleHeader: formattedHeader,
      curPlayer: '',
      tableData: null,
      tableDataTwo: null,
      searchPlayerOne: '',
      searchPlayerTwo: '',
      compareOne: '',
      compareTwo: '',
      curPage: page,
      searchPlayer: '',
      ironmanStatus: ironman,
      xpGainTime: timeframe,
      error: '',
    });
    this.apiXpTracker(section, page, timeframe, ironman);
    this.apiEndPage(ironman, section, true, timeframe);
  };

  setSection = (section, page, ironman) => {
    const formattedHeader = formatSectionName(section);
    this.setState({
      tableType: HIGHSCORES,
      curSection: section,
      titleHeader: formattedHeader,
      curPlayer: '',
      tableData: null,
      tableDataTwo: null,
      searchPlayerOne: '',
      searchPlayerTwo: '',
      compareOne: '',
      compareTwo: '',
      curPage: page,
      searchPlayer: '',
      error: '',
      ironmanStatus: ironman,
    });
    this.apiSkill(page, ironman, section);
    this.apiEndPage(ironman, section);
  };

  setPlayer = player => {
    this.setState({ playerXpGainIsLoading: true });
    this.setState({
      tableType: PLAYER,
      searchPlayer: player,
      curPlayer: player,
      curSection: null,
      tableData: null,
      tableDataTwo: null,
      searchPlayerOne: '',
      searchPlayerTwo: '',
      compareOne: '',
      compareTwo: '',
      ironmanStatus: 0,
      error: '',
    });
    this.apiPlayer(player, 'tableData');
  };

  setPlayers = (playerOne, playerTwo) => {
    this.setState({
      tableType: COMPARE,
      searchPlayer: '',
      searchPlayerOne: playerOne,
      searchPlayerTwo: playerTwo,
      compareOne: playerOne,
      compareTwo: playerTwo,
      titleHeader:
        formatPlayerName(playerOne) + ' & ' + formatPlayerName(playerTwo),
      curSection: null,
      curPlayer: null,
      tableData: null,
      tableDataTwo: null,
      ironmanStatus: 0,
      error: '',
    });
    this.apiPlayer(playerTwo, 'tableDataTwo');
    this.apiPlayer(playerOne, 'tableData');
  };

  setPage = page => {
    this.setState({ curPage: page });
    this.apiSkill(page, this.state.ironmanStatus, this.state.curSection);
  };

  setCompareFilter = compare => {
    this.setState({ compareFilter: compare });
  };

  apiSkill(page, im, skill) {
    const req = this.state.curRequest >= 1000 ? 0 : this.state.curRequest + 1;
    this.setState({ curRequest: req });
    axios
      .get(
        `https://vidyascape.org/api/highscores/skill/${skill}/${page}?ironman=${im}`,
        { timeout: API_TIMEOUT },
      )
      .then(response => {
        if (req !== this.state.curRequest) return;
        this.setState({ tableData: response.data });
      })
      .catch(error => {
        if (req !== this.state.curRequest) return;
        if (error.code === 'ECONNABORTED') {
          this.setState({ error: 408 });
        } else {
          this.setState({ error: error.response.status });
        }
      });
  }

  apiEndPage(im, skill, tracker, time) {
    const request =
      `https://vidyascape.org/api/` +
      (tracker
        ? `tracker/pages/${skill}?time=${time}&ironman=${im}`
        : `highscores/pages/${skill}?ironman=${im}`);
    axios
      .get(request)
      .then(response => {
        this.setState({ endPage: response.data.Pages });
      })
      .catch(error => {
        this.setState({ error: error.response.status });
      });
  }

  apiPlayer(player, table) {
    const req = this.state.curRequest >= 1000 ? 0 : this.state.curRequest + 1;
    this.setState({ curRequest: req });
    axios
      .get(`https://vidyascape.org/api/highscores/player/${player}`, {
        timeout: API_TIMEOUT,
      })
      .then(response => {
        if (req !== this.state.curRequest) return;
        this.setState({ [table]: response.data });
      })
      .catch(error => {
        if (req !== this.state.curRequest) return;
        if (error.code === 'ECONNABORTED') {
          this.setState({ error: 408 });
        } else {
          this.setState({ error: error.response.status });
        }
      });
  }

  apiPlayerXpChange(player, timeInSeconds) {
    XpGainApi.getPlayersXpInTime(player, timeInSeconds)
      .then(response => {
        this.setState({ playerXpGain: response }, () => {
          this.setState({ playerXpGainIsLoading: false });
        });
      })
      .catch(error => {
        this.setState({ error: error.response.status });
      });
  }

  apiXpTracker(skill, page, timeInSeconds, im) {
    const req = this.state.curRequest >= 1000 ? 0 : this.state.curRequest + 1;
    this.setState({ curRequest: req });
    axios
      .get(
        `https://vidyascape.org/api/tracker/skill/${skill}/${page}?time=${timeInSeconds}&ironman=${im}`,
        { timeout: API_TIMEOUT },
      )
      .then(response => {
        if (req !== this.state.curRequest) return;
        if (response.data) {
          this.setState({ tableData: response.data });
        } else {
          this.setState({ error: 204 });
        }
      })
      .catch(error => {
        if (req !== this.state.curRequest) return;
        if (error.code === 'ECONNABORTED') {
          this.setState({ error: 408 });
        } else {
          this.setState({ error: error.response.status });
        }
      });
  }

  componentDidMount() {
    this.checkParams();
  }

  componentDidUpdate() {
    this.checkParams();
  }

  checkParams() {
    //any state update here must be behind a self-satisfying if statement or you will continually update the state in an infinite loop
    const params = new URLSearchParams(this.props.location.search);
    if (params.has('player')) {
      if (params.has('compare')) {
        if (
          params.get('player') !== this.state.compareOne ||
          params.get('compare') !== this.state.compareTwo
        ) {
          this.setPlayers(params.get('player'), params.get('compare'));
        }
      } else {
        const player = params.get('player');
        const time = params.has('time') ? parseInt(params.get('time')) : 86400;

        if (player !== this.state.curPlayer || time !== this.state.xpGainTime) {
          if (player !== this.state.curPlayer) this.setPlayer(player);
          this.setState({ xpGainTime: time });
          this.setState({ playerXpGainIsLoading: true });
          this.apiPlayerXpChange(player, time);
        }
      }
    } else {
      const section = params.get('skill') ? params.get('skill') : 'overall';
      let iron = params.has('ironman')
        ? parseInt(params.get('ironman'), 10)
        : 0;
      let page = params.has('page') ? parseInt(params.get('page'), 10) : 1;
      let time = params.has('time') ? parseInt(params.get('time'), 10) : -1;
      iron = isNaN(iron) ? 0 : iron;
      page = isNaN(page) ? 1 : page;
      time = isNaN(time) ? -1 : time;
      if (time >= 0 && !getValidStats().includes(section)) {
        if (
          section !== this.state.curSection ||
          iron !== this.state.ironmanStatus ||
          time !== this.state.xpGainTime ||
          page !== this.state.curPage ||
          this.state.tableType !== TRACKER
        ) {
          this.setXpTracker(section, page, time, iron);
        }
      } else if (
        section !== this.state.curSection ||
        iron !== this.state.ironmanStatus ||
        this.state.tableType !== HIGHSCORES
      ) {
        this.setSection(section, page, iron);
      } else if (page !== this.state.curPage) {
        this.setPage(page);
      }
    }
  }

  generateSkillIcons() {
    const skills = getValidSkills(),
      stats = getValidStats();
    return [...skills, ...stats].map((s, k) => {
      return (
        <SkillIcon
          key={s + k}
          clickEvent={this.handleSectionClick}
          skill={s}
          active={this.state.curSection === s}
        />
      );
    });
  }

  getIronmanImage() {
    // TODO: this is a mess
    if (this.state.curPlayer && this.state.tableData) {
      if (this.state.tableData.ironman === 1) {
        return <img src={IronmanLgImg} alt="Ironman" title="Ironman" />;
      } else if (this.state.tableData.ironman === 3) {
        return (
          <img
            src={UltimateIronmanLgImg}
            alt="Ultimate Ironman"
            title="Ultimate Ironman"
          />
        );
      }
      return;
    }

    if (this.state.ironmanStatus === 1) {
      return <img src={IronmanLgImg} alt="Ironman" title="Ironman" />;
    } else if (this.state.ironmanStatus === 3) {
      return (
        <img
          src={UltimateIronmanLgImg}
          alt="Ultimate Ironman"
          title="Ultimate Ironman"
        />
      );
    }
  }

  render() {
    let displayTable = this.state.error ? (
      <Error code={this.state.error} />
    ) : this.state.tableType === NO_RESP ? (
      <Error code={204} />
    ) : (
      <SkeletonTable
        stat={getValidStats().includes(this.state.curSection)}
        mode={this.state.tableType}
      />
    );

    if (this.state.tableData) {
      if (this.state.tableType === COMPARE) {
        if (this.state.tableDataTwo) {
          displayTable = (
            <CompareTable
              compareFilter={this.state.compareFilter}
              clickEvent={this.handleSectionClick}
              clickEventPlayer={this.handlePlayerClick}
              data={this.state.tableData}
              dataTwo={this.state.tableDataTwo}
            />
          );
        } else {
          // Error
        }
      } else if (this.state.tableType === PLAYER) {
        if (this.state.curPlayer) {
          displayTable = (
            <PlayerTable
              clickEvent={this.handleSectionClick}
              data={this.state.tableData}
              xpGain={this.state.playerXpGain}
              xpGainIsLoading={this.state.playerXpGainIsLoading}
            />
          );
        }
      } else if (this.state.tableType === TRACKER) {
        if (this.state.curSection) {
          displayTable = (
            <Aux>
              <XPTrackerTable
                clickEvent={this.handlePlayerClick}
                page={this.state.curPage}
                skill={this.state.curSection}
                data={this.state.tableData}
              />
              <PageNavigation
                page={this.state.curPage}
                pages={populatePages}
                endPage={this.state.endPage}
                click={this.handlePageClick}
              />
            </Aux>
          );
        }
      } else if (this.state.tableType === HIGHSCORES) {
        if (getValidStats().includes(this.state.curSection)) {
          displayTable = (
            <Aux>
              <StatTable
                clickEvent={this.handlePlayerClick}
                page={this.state.curPage}
                stat={this.state.curSection}
                data={this.state.tableData}
              />
              <PageNavigation
                page={this.state.curPage}
                pages={populatePages}
                endPage={this.state.endPage}
                click={this.handlePageClick}
              />
            </Aux>
          );
        } else if (this.state.curSection) {
          displayTable = (
            <Aux>
              <SkillTable
                clickEvent={this.handlePlayerClick}
                page={this.state.curPage}
                skill={this.state.curSection}
                data={this.state.tableData}
              />
              <PageNavigation
                page={this.state.curPage}
                pages={populatePages}
                endPage={this.state.endPage}
                click={this.handlePageClick}
              />
            </Aux>
          );
        }
      }
    }

    return (
      <Aux>
        <Header
          ironmanIcon={this.getIronmanImage()}
          title={
            this.state.curPlayer
              ? formatPlayerName(this.state.curPlayer)
              : this.state.titleHeader
          }
          skillIcons={this.generateSkillIcons()}
        />

        <Row>
          <Col sm={3} md={3} lg={3} xl={3}>
            <Sidebar
              mode={this.state.tableType}
              iron={this.state.ironmanStatus}
              ironmanOnClick={this.handleIronmanClick}
              compareFilterOnClick={this.setCompareFilter}
              playerOnSubmit={this.handlePlayerSubmit}
              compareOnSubmit={this.handleCompareSubmit}
              searchPlayerOne={this.state.searchPlayerOne}
              searchPlayerTwo={this.state.searchPlayerTwo}
              playerOnChange={this.handlePlayerChange}
              searchPlayer={this.state.searchPlayer}
              currentTimeFrame={this.state.xpGainTime}
              xpGainTimeChange={this.handleXpGainTimeChange}
              xpTrackerOnClick={this.handleXpTrackerClick}
              data={this.state.tableData}
              compareFilter={this.state.compareFilter}
              section={this.state.curSection}
            />
          </Col>
          <Col sm={9} md={9} lg={9} xl={9}>
            {displayTable}
          </Col>
        </Row>
      </Aux>
    );
  }
}
