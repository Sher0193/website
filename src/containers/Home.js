import React, { Component } from 'react';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import Aux from '../hoc/Auxiliary';
import Patchnotes from '../components/Home/Patchnotes/Patchnotes';
import About from '../components/Home/About/About';
import Announcements from '../components/Home/Announcements/Announcements';
import { vscapeFileRoot } from '../utils/constants';
import axios from '../axios';

export default class Home extends Component {
  state = {
    patchnotes: '',
    announcements: '',
  };

  componentDidMount() {
    axios.get(vscapeFileRoot + 'patchnotes.json').then(response => {
      this.setState({ patchnotes: response.data.splice(0, 5) });
    });

    axios.get(vscapeFileRoot + 'announcements.json').then(response => {
      this.setState({ announcements: response.data.splice(0, 5) });
    });
  }

  render() {
    return (
      <Aux>
        <About />

        <Row>
          <Col xs={12} sm={12} md={12} lg={6}>
            <Announcements announcements={this.state.announcements} />
          </Col>
          <Col xs={12} sm={12} md={12} lg={6}>
            <Patchnotes patchnotes={this.state.patchnotes} />
          </Col>
        </Row>
      </Aux>
    );
  }
}
