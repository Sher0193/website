import React from 'react';
import 'leaflet/dist/leaflet.css';

import Map from 'react-leaflet/lib/Map';
import ImageOverlay from 'react-leaflet/lib/ImageOverlay';
import Leaflet from 'leaflet';

import MapImg from '../assets/images/map.png';

//Size of the map image
const bounds = [[0, 0], [5888, 7424]];

//Pad the map image with an extra region
const maxBounds = [[-256, -256], [bounds[1][0] + 256, bounds[1][1] + 256]];

const map = props => (
  <Map
    // Stylish!
    style={{ background: 'transparent' }}
    // Assign bounds vars
    bounds={bounds}
    maxBounds={maxBounds}
    center={[bounds[0][0] / 2, bounds[0][1] / 2]}
    // Prevents out of bounds dragging strictly
    maxBoundsViscosity={1.0}
    // Turns the map into a non-geographical map
    crs={Leaflet.CRS.Simple}
    // Some control settings
    zoomControl={true}
    attributionControl={false}
    // Settings for how much the map can be zoomed
    minZoom={-3}
    maxZoom={0}
    zoomAnimation={false}
    zoomSnap={0.25}
    // Disables map dragging having a delay
    inertia={false}
  >
    <ImageOverlay url={MapImg} bounds={bounds} />
  </Map>
);

export default map;
