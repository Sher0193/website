import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';

import Aux from '../hoc/Auxiliary';
import Checksum from '../components/Downloads/Checksum';
import StaticCard from '../components/UI/StaticCard';
import { vscapeFileRoot } from '../utils/constants';

import axios from '../axios';

export default class Downloads extends Component {
  state = {
    launcherSHA256: 'Loading...',
    launcherSHA1: 'Loading...',
    launcherMD5: 'Loading...',
    clientSHA256: 'Loading...',
    clientSHA1: 'Loading...',
    clientMD5: 'Loading...',
  };

  getChecksum() {
    const files = [
      { file: 'vidyascape_launcher.jar.sha256', prop: 'launcherSHA256' },
      { file: 'vidyascape_launcher.jar.sha1', prop: 'launcherSHA1' },
      { file: 'vidyascape_launcher.jar.md5', prop: 'launcherMD5' },
      { file: 'client/vidyascape.jar.sha256', prop: 'clientSHA256' },
      { file: 'client/vidyascape.jar.sha1', prop: 'clientSHA1' },
      { file: 'client/vidyascape.jar.md5', prop: 'clientMD5' },
    ];
    for (const f of files) {
      axios.get(vscapeFileRoot + f.file).then(response => {
        this.setState({ [f.prop]: response.data });
      });
    }
  }

  componentDidMount() {
    this.getChecksum();
  }

  cardHeader(title) {
    return <strong>{title}</strong>;
  }

  cardBody(dlLink, descrip, sha256, sha1, md5) {
    return (
      <Aux>
        <Button variant="warning" size="lg" href={dlLink}>
          Download
        </Button>
        <br />
        <br />
        <p>{descrip}</p>
        {sha256 ? <Checksum title={'SHA256'} checksum={sha256} /> : ''}
        {sha1 ? <Checksum title={'SHA1'} checksum={sha1} /> : ''}
        {md5 ? <Checksum title={'MD5'} checksum={md5} /> : ''}
      </Aux>
    );
  }

  render() {
    return (
      <Aux>
        <StaticCard
          title={this.cardHeader('Launcher (Recommended)')}
          headerClasses={'text-center'}
          bodyClasses={'text-center'}
        >
          {this.cardBody(
            vscapeFileRoot + 'vidyascape_launcher.jar',
            'The launcher will auto-update every time a new client is released. It also displays recent patchnotes.',
            this.state.launcherSHA256,
            this.state.launcherSHA1,
            this.state.launcherMD5,
          )}
        </StaticCard>

        <StaticCard
          title={this.cardHeader('Client')}
          headerClasses={'text-center'}
          bodyClasses={'text-center'}
        >
          {this.cardBody(
            vscapeFileRoot + 'client/vidyascape.jar',
            'Just the client. It will need to be manually updated when a new client release occurs.',
            this.state.clientSHA256,
            this.state.clientSHA1,
            this.state.clientMD5,
          )}
        </StaticCard>
      </Aux>
    );
  }
}
