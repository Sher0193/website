import React, { Component } from 'react';

import Aux from '../hoc/Auxiliary';
import MarkdownRender from '../components/UI/MarkdownRender';
import StaticCard from '../components/UI/StaticCard';
import CollapsableCard from '../components/UI/CollapsableCard';
import ScrollTo from '../components/UI/ScrollTo';
import { vscapeFileRoot } from '../utils/constants';

import axios from '../axios';

export default class DevBlog extends Component {
  state = {
    posts: [],
    scrollIndex: null,
  };

  togglePost = propID => {
    const postIndex = Number(propID.split('-')[1]);
    if (this.state.posts[postIndex].body) {
      this.updatePosts(postIndex);
      return;
    }
    axios
      .get(vscapeFileRoot + `devblog/${this.state.posts[postIndex].file}`)
      .then(resp => {
        this.updatePosts(postIndex, resp.data);
      });
  };

  updatePosts(postIndex, body) {
    const newPosts = this.state.posts.map((p, i) => {
      if (postIndex === i) {
        p.isOpen = !p.isOpen;
        if (!p.body && body) {
          p.body = body;
        }
      }
      return p;
    });
    this.setState({ posts: newPosts });
  }

  componentDidMount() {
    axios
      .get(vscapeFileRoot + 'devblog/posts.json') // Get the json of all posts
      .then(response => {
        let file = response.data[0].file;
        const hash = window.location.hash.substring(1);
        if (hash) {
          file = hash + '.md';
        }
        axios
          .get(vscapeFileRoot + `devblog/${file}`) // Get the first post markdown/body since it's open be default
          .then(bodyResp => {
            const hash = window.location.hash.substring(1);
            let scrollIndex = null;
            const newPosts = response.data.map((p, i) => {
              if (hash) {
                if (p.file.split('.')[0] === hash) {
                  scrollIndex = i;
                  p.body = bodyResp.data;
                  p.isOpen = true;
                }
              } else {
                p.isOpen = i === 0;
                p.body = i === 0 ? bodyResp.data : null;
              }
              return p;
            });
            this.setState({ posts: newPosts, scrollIndex: scrollIndex });
          });
      });
  }

  displayPosts() {
    return this.state.posts.map((p, i) => {
      const parsedBody = p.body ? <MarkdownRender markdown={p.body} /> : '';
      return (
        <ScrollTo
          index={i}
          key={'post-scroll-to-' + i}
          scrollToIndex={this.state.scrollIndex}
        >
          <CollapsableCard
            toggle={this.togglePost}
            isOpen={p.isOpen}
            title={`${p.title} - ${p.date}`}
            body={parsedBody}
            id={`post-${i}`}
            key={`post-${i}`}
          />
        </ScrollTo>
      );
    });
  }

  render() {
    return (
      <Aux>
        <StaticCard
          title={<strong>Dev Blog</strong>}
          headerClasses={'text-center'}
        >
          {this.displayPosts()}
        </StaticCard>
      </Aux>
    );
  }
}
