import React from 'react';

import IronmanImg from '../assets/images/icons/ironman.png';
import UltimateIronmanImg from '../assets/images/icons/ultimate.png';

/**
 * Adds digit group separators to numbers (e.g. 10000 would return
 * 10,000)
 * @param {number} n Number to add digit group separators.
 */
export const formatNumber = n => {
  if (typeof n === 'number') {
    return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
  }
  return 0;
};

/**
 * Return img tag with correct ironman helm icon for the given type.
 * 1 is ironman, 3 is ultimate ironman.
 * @param {number} type
 */
export const getIronmanIcon = type => {
  if (type === 1) {
    return <img src={IronmanImg} alt="Ironman" title="Ironman" />;
  } else if (type === 3) {
    return (
      <img
        src={UltimateIronmanImg}
        alt="Ultimate Ironman"
        title="Ultimate Ironman"
      />
    );
  }
};

export const getValidSkills = () => {
  return [
    'overall',
    'attack',
    'defence',
    'strength',
    'hitpoints',
    'ranged',
    'prayer',
    'magic',
    'cooking',
    'woodcutting',
    'fletching',
    'fishing',
    'firemaking',
    'crafting',
    'smithing',
    'mining',
    'herblore',
    'agility',
    'thieving',
    'slayer',
    'farming',
    'runecrafting',
    'hunter',
    'construction',
  ];
};

export const getValidBosses = () => {
  return [
    'dag_supreme',
    'dag_prime',
    'dag_rex',
    'giant_mole',
    'kq',
    'kbd',
    'chaos_ele',
    'jad',
    'graardor',
    'kril',
    'zilyana',
  ];
};

/**
 * Returns array boss names. Odd indices correspond to api response, even indices are proper names.
 * Response name index + 1 = proper name index.
 */
export const getBossNameMap = () => {
  return [
    'dag_supreme',
    'Dagannoth Supreme',
    'dag_prime',
    'Dagannoth Prime',
    'dag_rex',
    'Dagannoth Rex',
    'giant_mole',
    'Giant Mole',
    'kq',
    'Kalphite Queen',
    'kbd',
    'King Black Dragon',
    'chaos_ele',
    'Chaos Elemental',
    'jad',
    'Tz-Tok Jad',
    'graardor',
    'General Graardor',
    'kril',
    "K'ril Tsutsaroth",
    'zilyana',
    'Commander Zilyana',
  ];
};

export const getValidStats = () => {
  return ['easy_clues', 'medium_clues', 'hard_clues'];
};

/** Formats section names.
 * Each section name is capitalized initially, and following a space.
 * Underscores are converted to spaces.
 * @param {string} s Section name to be formatted.
 */
export const formatSectionName = s => {
  if (s) {
    if (getBossNameMap().includes(s)) {
      for (let i = 0; i < getBossNameMap().length; i++) {
        if (getBossNameMap()[i] === s) return getBossNameMap()[i + 1];
      }
    } else {
      return s.split('_').map(word => {
        return word
          .split('')
          .map((c, i) => {
            return i === 0 ? ` ${c.toUpperCase()}` : `${c}`;
          })
          .join('');
      });
    }
  }
};

/** Formats player names.
 * Each player name is capitalized initially, and following a space or underscore.
 * @param {string} s Player name to be formatted.
 */
export const formatPlayerName = s => {
  if (s) {
    let str = '';
    const arr = s.toLowerCase().split(' ');
    // Capitalize each string segmented by a space
    for (const i in arr) {
      const arrb = arr[i].split('_');
      let strb = '';
      // Capitalize each string segmented by an underscore within a space segment
      for (const x in arrb) {
        // Replace the underscore between segments
        const uCase = arrb[x].charAt(0).toUpperCase() + arrb[x].substr(1);
        strb += x > 0 ? `_${uCase}` : `${uCase}`;
      }
      // Replace the space between segments
      str += i > 0 ? ` ${strb}` : `${strb}`;
    }
    return str;
  }
};

export const calculateCombatLevel = data => {
  if (data) {
    const melee = Math.floor(
      0.25 *
        (data['defence_lvl'] +
          data['hitpoints_lvl'] +
          Math.floor(data['prayer_lvl'] / 2)) +
        0.325 * (data['attack_lvl'] + data['strength_lvl']),
    );
    const ranged = Math.floor(
      0.25 *
        (data['defence_lvl'] +
          data['hitpoints_lvl'] +
          Math.floor(data['prayer_lvl'] / 2)) +
        0.325 * (Math.floor(data['ranged_lvl'] / 2) + data['ranged_lvl']),
    );
    const magic = Math.floor(
      0.25 *
        (data['defence_lvl'] +
          data['hitpoints_lvl'] +
          Math.floor(data['prayer_lvl'] / 2)) +
        0.325 * (Math.floor(data['magic_lvl'] / 2) + data['magic_lvl']),
    );
    if (melee > ranged && melee > magic) {
      return melee;
    } else if (ranged > magic) {
      return ranged;
    } else {
      return magic;
    }
  }
  return 3;
};

export const calculateProgression = data => {
  return {
    exp: data ? Math.floor((data['overall_xp'] / 4600000000) * 100) : 0,
    lvl: data ? Math.floor((data['overall_lvl'] / 2277) * 100) : 0,
  };
};

export const populatePages = (page, endPage) => {
  const array =
    endPage < 10
      ? Array.from(Array(endPage).keys())
      : Array.from(Array(10).keys());
  let i = 0;
  if (page + 4 > endPage) {
    for (const x in array) {
      array[x] = i + (endPage - (array.length - 1));
      i++;
    }
  } else if (page < 6) {
    for (const x in array) {
      array[x] = i + 1;
      i++;
    }
  } else {
    for (const x in array) {
      array[x] = i + page - 5;
      i++;
    }
  }
  return array;
};
